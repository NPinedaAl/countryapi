module.exports = {
    responseError,
    responseSuccess
}

function responseError(res, errors, status=500) {
    return res.status(status).json({ success: false, errors: errors })
}

function responseSuccess(res, data, status=200) {
    return res.status(status).json({ success: true, data: data })
}