const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const validator = require('express-validator')
const models = require('./models')
const countryRouter = require('./routes').countryRouter
const subdivisionRouter = require('./routes').subdivisionRouter

// models.sequelize.sync({ force: true })

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(validator())
app.use(cors())

app.get('/api', (req, res) => res.status(200).send({
  message: 'Country Api'
}))

app.use('/api/countries', countryRouter)
app.use('/api/countries/:parentid/subdivisions', subdivisionRouter)

module.exports = app