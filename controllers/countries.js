const { param, body } = require('express-validator/check')
const { sanitize, sanitizeBody } = require('express-validator/filter')
const { validationResult } = require('express-validator/check')
const Op = require('sequelize').Op
const { responseError, responseSuccess } = require('../globals')

module.exports = function (Country) {
  return {
    list(req, res) {
      var query = {}
      if (req.query.name) {
        query.where = { short_name: { [Op.like]: `%${req.query.name}%` } }
      }
      Country.findAll(query)
        .then(models => responseSuccess(res, models))
        .catch(error => responseError(res, { description: error.name }))
    },
    create(req, res) {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return responseError(res, errors.mapped(), 422)
      }
      Country.create({
        alpha_2_code,
        alpha_3_code,
        numeric_code,
        short_name,
        full_name,
      } = req.body)
        .then(model => responseSuccess(res, model, 201))
        .catch(error => responseError(res, { description: error.name }))
    },
    find(req, res, next) {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return responseError(res, errors.mapped(), 422)
      }
      Country.findById(req.params.id, { rejectOnEmpty: true })
        .then(model => {
          req.parent = model
          req.model = model
          next()
        })
        .catch(error => responseError(res, { description: 'Country Not Found' }, 404))
    },
    show(req, res) {
      responseSuccess(res, req.model)
    },
    update(req, res) {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return responseError(res, errors.mapped(), 422)
      }
      req.model.update({
        alpha_2_code,
        alpha_3_code,
        numeric_code,
        short_name,
        full_name,
      } = req.body)
        .then(model => responseSuccess(res, model))
        .catch(error => responseError(res, { description: error.name }))
    },
    delete(req, res) {
      req.model.destroy()
        .then(() => responseSuccess(res, []))
        .catch(error => responseError(res, { description: error.name }))
    },
    validateParam: [
      param('id').isNumeric().withMessage('The field must be Numeric')
    ],
    validateCreate: [
      body('alpha_2_code')
        .exists().withMessage('The field is required')
        .isAlpha().withMessage('Field must be Alphabetic')
        .isLength({ min: 2, max: 2 }).withMessage('Length field must be 2')
        .custom(value => {
          return Country.findOne({ where: { alpha_2_code: value } })
            .then(model => { if (model) throw new Error('This Code is already in use') })
        }),
      body('alpha_3_code')
        .exists().withMessage('The field is required')
        .isAlpha().withMessage('Field must be Alphabetic')
        .isLength({ min: 3, max: 3 }).withMessage('Length field must be 3')
        .custom(value => {
          return Country.findOne({ where: { alpha_3_code: value } })
            .then(model => { if (model) throw new Error('This Code is already in use') })
        }),
      body('numeric_code')
        .exists().withMessage('The field is required')
        .isNumeric().withMessage('The field must be Numeric')
        .custom(value => {
          return Country.findOne({ where: { numeric_code: value } })
            .then(model => { if (model) throw new Error('This Code is already in use') })
        }),
      body('short_name')
        .exists().withMessage('The field is required')
        .isLength({ min: 3, max: 255 }).withMessage('Length field must be at least 3')
    ],
    validateUpdate: [
      body('alpha_2_code')
        .optional()
        .isAlpha().withMessage('Field must be Alphabetic')
        .isLength({ min: 2, max: 2 }).withMessage('Length field must be 2')
        .custom((value, { req }) => {
          if (value !== req.model.alpha_2_code) {
            return Country.findOne({ where: { alpha_2_code: value } })
              .then(model => { if (model) throw new Error('This Code is already in use') })
          }
          return true
        }),
      body('alpha_3_code')
        .optional()
        .isAlpha().withMessage('Field must be Alphabetic')
        .isLength({ min: 3, max: 3 }).withMessage('Length field must be 3')
        .custom((value, { req }) => {
          if (value !== req.model.alpha_3_code) {
            return Country.findOne({ where: { alpha_3_code: value } })
              .then(model => { if (model) throw new Error('This Code is already in use') })
          }
          return true
        }),
      body('numeric_code')
        .optional()
        .isNumeric().withMessage('The field must be Numeric')
        .custom((value, { req }) => {
          if (value !== req.model.numeric_code) {
            return Country.findOne({ where: { numeric_code: value } })
              .then(model => { if (model) throw new Error('This Code is already in use') })
          }
          return true
        }),
      body('short_name')
        .optional()
        .isLength({ min: 3, max: 255 }).withMessage('Length field must be at least 3')
    ]
  }
}