const countriesController = require('./countries')
const subdivisionsController = require('./subdivisions')

module.exports = {
  countriesController,
  subdivisionsController
}