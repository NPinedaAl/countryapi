const { param, body } = require('express-validator/check')
const { sanitize, sanitizeBody } = require('express-validator/filter')
const { validationResult } = require('express-validator/check')
const { responseError, responseSuccess } = require('../globals')

module.exports = function (Subdivision) {
  return {
    list(req, res) {
      Subdivision.findAll({ where: { CountryId: req.parent.id } })
        .then(models => responseSuccess(res, models))
        .catch(error => responseError(res, { description: error.name }))
    },
    create(req, res) {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return responseError(res, errors.mapped(), 422)
      }
      Subdivision.create({
        code: req.body.code,
        name: req.body.name,
        CountryId: req.parent.id
      })
        .then(model => responseSuccess(res, model, 201))
        .catch(error => responseError(res, { description: error.name }))
    },
    find(req, res, next) {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return responseError(res, errors.mapped(), 422)
      }
      Subdivision.findById(req.params.idsub, { rejectOnEmpty: true })
        .then(model => {
          req.model = model
          next()
        })
        .catch(error => responseError(res, { description: 'Subdivision Not Found' }, 404))
    },
    show(req, res) {
      responseSuccess(res, req.model)
    },
    update(req, res) {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return responseError(res, errors.mapped(), 422)
      }
      req.model.update({
        code,
        name,
      } = req.body)
        .then(model => responseSuccess(res, model))
        .catch(error => responseError(res, { description: error.name }))
    },
    delete(req, res) {
      req.model.destroy()
        .then(model => responseSuccess(res, model))
        .catch(error => responseError(res, { description: error.name }))
    },
    validateParam: [
      param('idsub').isNumeric().withMessage('The field must be Numeric')
    ],
    validateCreate: [
      body('code')
        .exists().withMessage('The field is required')
        .matches(/^[A-Z]{2}-[A-Z0-9]{1,3}$/).withMessage('The field must be Alphanumeric AA-XXX')
        .custom(value => {
          return Subdivision.findOne({ where: { code: value } })
            .then(model => { if (model) throw new Error('This Code is already in use') })
        }),
      body('name')
        .exists().withMessage('The field is required')
        .isLength({ min: 3, max: 255 }).withMessage('Length field must be at least 3')
    ],
    validateUpdate: [
      body('code')
        .matches(/^[A-Z]{2}-[A-Z0-9]{1,3}$/).withMessage('The field must be Alphanumeric AA-XXX')
        .custom((value, { req }) => {
          if (value !== req.model.code) {
            return Subdivision.findOne({ where: { code: value } })
              .then(model => { if (model) throw new Error('This Code is already in use') })
          }
          return true
        }),
      body('name')
        .isLength({ min: 3, max: 255 }).withMessage('Length field must be at least 3')
    ]
  }
}