'use strict';
module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    alpha_2_code: { type: DataTypes.STRING(2), allowNull: false, unique: true },
    alpha_3_code: { type: DataTypes.STRING(3), allowNull: false, unique: true },
    numeric_code: { 
      type: DataTypes.STRING, allowNull: false, unique: true, 
      validate: {
        isNumeric: true
    }},
    short_name: { type: DataTypes.STRING, allowNull: false },
    full_name: DataTypes.STRING
  });
  // Class Method
  Country.associate = (models) => {
    // associations can be defined here
    Country.hasMany(models.Subdivision, {foreignKey: { allowNull: false }});
  };
  
  return Country;
};
