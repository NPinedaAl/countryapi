'use strict';
module.exports = (sequelize, DataTypes) => {
  const Subdivision = sequelize.define('Subdivision', {
    code: DataTypes.STRING,
    name: DataTypes.STRING
  });
  // Class Method
  Subdivision.associate = (models) => {
    Subdivision.belongsTo(models.Country);
  };
  
  return Subdivision;
};