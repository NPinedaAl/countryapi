'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Subdivisions', [
      {
        code: 'GT-AV',
        name: 'Alta Verapaz',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-BV',
        name: 'Baja Verapaz',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-CM',
        name: 'Chimaltenango',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-CQ',
        name: 'Chiquimula',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-PR',
        name: 'El Progreso',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-ES',
        name: 'Escuintla',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-GU',
        name: 'Guatemala',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-HU',
        name: 'Huehuetenango',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-IZ',
        name: 'Izabal',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-JA',
        name: 'Jalapa',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-JU',
        name: 'Jutiapa',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-PE',
        name: 'Peten',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-QZ',
        name: 'Quetzaltenango',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-QC',
        name: 'Quiche',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-RE',
        name: 'Retalhuleu',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-SA',
        name: 'Sacatepequez',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-SM',
        name: 'San Marcos',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-SR',
        name: 'Santa Rosa',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-SO',
        name: 'Solola',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-SU',
        name: 'Suchitepequez',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-TO',
        name: 'Totonicapan',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'GT-ZA',
        name: 'Zacapa',
        CountryId: 1,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-AH',
        name: 'Ahuachapan',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-CA',
        name: 'Cabañas',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-CU',
        name: 'Cuscatlan',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-CH',
        name: 'Chalatenango',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-LI',
        name: 'La Libertad',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-PA',
        name: 'La Paz',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-UN',
        name: 'La Union',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-MO',
        name: 'Morazan',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-SM',
        name: 'San Miguel',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-SS',
        name: 'San Salvador',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-SA',
        name: 'Santa Ana',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-SV',
        name: 'San Vicente',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-SO',
        name: 'Sonsonate',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        code: 'SV-US',
        name: 'Usulutan',
        CountryId: 2,
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },

    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Subdivisions', null, {});
  }
};
