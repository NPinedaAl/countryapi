'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Countries', [
      {
        id: 1,
        alpha_2_code: 'GT',
        alpha_3_code: 'GTM',
        numeric_code: '320',
        short_name: 'Guatemala',
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        id: 2,
        alpha_2_code: 'SV',
        alpha_3_code: 'SLV',
        numeric_code: '222',
        short_name: 'El Salvador',
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        alpha_2_code: 'BR',
        alpha_3_code: 'BRA',
        numeric_code: '076',
        short_name: 'Brazil',
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        alpha_2_code: 'US',
        alpha_3_code: 'USA',
        numeric_code: '840',
        short_name: 'United States of America',
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      },
      {
        alpha_2_code: 'RU',
        alpha_3_code: 'RUS',
        numeric_code: '643',
        short_name: 'Russian Federation',
        createdAt: '2018-02-19 08:24:44',
	      updatedAt: '2018-02-19 08:24:44'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('Countries', null, {});
  }
};
