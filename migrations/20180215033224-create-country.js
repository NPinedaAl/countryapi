'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Countries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      alpha_2_code: {
        type: Sequelize.STRING(2),
        allowNull: false,
        unique: true,
      },
      alpha_3_code: {
        type: Sequelize.STRING(3),
        allowNull: false,
        unique: true
      },
      numeric_code: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      short_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      full_name: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Countries');
  }
};