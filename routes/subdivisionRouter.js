var express = require('express')
const Subdivision = require('../models').Subdivision
const subdivisionsController = require('../controllers').subdivisionsController(Subdivision)

var router = express.Router()

router.route('/')
  .get(subdivisionsController.list)
  .post(subdivisionsController.validateCreate, subdivisionsController.create)

router.use('/:idsub', subdivisionsController.validateParam, subdivisionsController.find)

router.route('/:idsub')
  .get(subdivisionsController.show)
  .put(subdivisionsController.validateUpdate, subdivisionsController.update)
  .delete(subdivisionsController.delete)

module.exports = router