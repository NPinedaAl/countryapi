const countryRouter = require('./countryRouter')
const subdivisionRouter = require('./subdivisionRouter')

module.exports = {
    countryRouter,
    subdivisionRouter
}