var express = require('express')
const Country = require('../models').Country
const countriesController = require('../controllers').countriesController(Country)

var router = express.Router()

router.route('/')
  .get(countriesController.list)
  .post(countriesController.validateCreate, countriesController.create)

router.use('/:id', countriesController.validateParam, countriesController.find)

router.route('/:id')
  .get(countriesController.show)
  .put(countriesController.validateUpdate, countriesController.update)
  .delete(countriesController.delete)

module.exports = router