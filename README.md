# Rest Api Countries

## Description
REST API for Countries and Subdivisions management. Based on ISO_366-1 Standard

##### Server          : Nodejs 6.x
##### Routing         : Express 4.x
##### ORM Database    : Sequelize 4.x
##### Database        : MySql 2.x

## Installation

#### Donwload Code | Clone the Repo

```
git clone https://NPinedaAl@bitbucket.org/NPinedaAl/countryapi.git
```

#### Install Node Modules
```
npm install
```

#### Edit DB Params
You will edit DataBase connection parameters accordingly to your needs in the file config/config.json

#### Demo Data
```
npm run migrate
npm run seed
```

#### Start Demo Server
```
npm start
```
Local Server is running on port 3000

#### Api Links
Get List of Countries
http://localhost:3000/api/countries

Create a New Country
Send POST request to
http://localhost:3000/api/countries
With Body json, for example:
``` 
{
    "alpha_2_code": "DE",
    "alpha_3_code": "DEU",
    "numeric_code": "999",
    "short_name": "Germany"
}
```

Search for a Country
http://localhost:3000/api/countries?name=Guatemala

Get a Country
http://localhost:3000/api/countries/1

Update a Country
Send PUT request to same link to get a country
http://localhost:3000/api/countries/1
With Body json including only the fields to update
``` 
{
    "numeric_code": "276",
    "short_name": "Prussian Empire"
}
```

Delete a Country
Send DELETE request to same link to get a Country
http://localhost:3000/api/countries/1


List Country Subdivisions
http://localhost:3000/api/countries/1/subdivisions


Create a New Subdivision
Send POST request to Country Subdivisions link
http://localhost:3000/api/countries/99/subdivisions
With Body json
``` 
{
    "code": "DE-BY",
    "name": "BAYERN"
}
```
Get a Country Subdivision
http://localhost:3000/api/countries/1/subdivisions/1

Update a Country Subdivision
Send PUT request to same link to get a Country Subdivision
http://localhost:3000/api/countries/1/subdivisions/1
With Body json including only the fields to update

Delete a Country Subdivision
Send DELETE request to same link to get a Country Subdivision
http://localhost:3000/api/countries/1/subdivisions/1
